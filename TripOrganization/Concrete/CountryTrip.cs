﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TripOrganization.Interfaces;
using TripOrganization.Patterns.Strategy;

namespace TripOrganization.Concrete
{
    public class CountryTrip : ITrip
    {
        public CountryTrip()
        {
        }

        decimal ITrip.CalculateTripPrice(Calculation calcDriveCostParam, int personCount, int days)
        {
            decimal driveCost = calcDriveCostParam.CalculateCost();
            decimal journeyPrice = 0;

            journeyPrice = driveCost + (personCount * 30 * days); //koszty podróży środkiem transportu + hotel

            return journeyPrice;
        }

        int ITrip.CalculateTripTime(Calculation calcDriveTimeParam, int days)
        {
            int journeyTimeHour = calcDriveTimeParam.CalculateTimeTrip() + (days * 24);
            return journeyTimeHour;
        }

    }
}