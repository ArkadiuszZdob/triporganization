﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TripOrganization.Interfaces;
using TripOrganization.Patterns.Strategy;

namespace TripOrganization.Concrete
{
    public class AbroadTrip : ITrip
    {
        public AbroadTrip()
        {
        }

        decimal ITrip.CalculateTripPrice(Calculation calcDriveCostParam, int personCount, int days)
        {
            decimal driveCost = calcDriveCostParam.CalculateCost();
            decimal journeyPrice = 0;

            journeyPrice=driveCost+(personCount*50*days)+100m; //koszty podróży środkiem transportu + hotel + ubezpieczenie

            return journeyPrice;
        }

        int ITrip.CalculateTripTime(Calculation calcDriveTimeParam,int days)
        {
            int journeyTimeHour = calcDriveTimeParam.CalculateTimeTrip() + (days * 24);
            return journeyTimeHour;
        }
    }
}