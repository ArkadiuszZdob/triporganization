﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TripOrganization.Interfaces;
using TripOrganization.Concrete.Base;

namespace TripOrganization.Concrete
{
    public class DBManager : IManageBase
    {
        private TripBaseEntities EFTrip;
        public DBManager()
        {
            EFTrip = new TripBaseEntities();
        }

        bool IManageBase.SaveTrip(TripReservation reservationParam)
        {
            try
            {
                EFTrip.TripReservation.Add(reservationParam);
                EFTrip.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        IEnumerable<TripReservation> IManageBase.GetAllTrips()
        {
            return EFTrip.TripReservation;
        }
    }
}