﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TripOrganization.Patterns.Strategy;
using TripOrganization.Models;
using System.Web.Mvc;
using TripOrganization.Interfaces;
using TripOrganization.Patterns.Factory;
using TripOrganization.Patterns.Adapter;

namespace TripOrganization.Controllers
{
    public class HomeController : Controller
    {
        private TripFactory factory;
        private ITrip tripManager;
        private Trip_type tripTypeC;
        private IManageBase _managerDB;
        public HomeController(IManageBase manageParam)
        {
            factory = new TripFactory();
            _managerDB = manageParam;
        }

        [HttpGet]
        /// <summary>
        /// Wybranie typu wycieczki
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        /// <summary>
        /// Przetwarzanie typu wycieczki
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(string tripType)
        {
            if (tripType == "1")
            {
                tripManager = factory.CreateTripManager(Models.Trip_type.COUNTRY);
                tripTypeC = Trip_type.COUNTRY;
                return RedirectToAction("CreateTrip");
            }
            else if(tripType=="2")
            {
                tripManager = factory.CreateTripManager(Models.Trip_type.ABROAD);
                tripTypeC = Trip_type.ABROAD;
                return RedirectToAction("CreateTrip");
            }

            TempData["Message"] = "Nie udało się poprawnie określić typu wycieczki. Spróbuj jeszcze raz !";
            return View();
        }

        [HttpGet]
        public ActionResult CreateTrip()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult CreateTrip(TripInformation tripParam)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Calculation methodtoCalc = GetCalculationPattern(GetTransportationType(tripParam.TransportationType),
                        Int32.Parse(tripParam.Kilometers),Int32.Parse(tripParam.PeopleCount));

                    TripInformation currentTrip = new TripInformation() {
                        DateFrom = tripParam.DateFrom,
                        DateTo = tripParam.DateTo,
                        TransportationType = tripParam.TransportationType,
                        Description = tripParam.Description ?? "---",
                        Kilometers = tripParam.Kilometers,
                        PeopleCount = tripParam.PeopleCount,
                        PersonReservation = tripParam.PersonReservation,
                        PlaceTrip = tripParam.PlaceTrip,
                        Telephone = tripParam.Telephone,
                        TravelHour = methodtoCalc.CalculateTimeTrip(),
                        TripCost = methodtoCalc.CalculateCost().ToString()
                    };

                    return RedirectToAction("ConfirmTrip",currentTrip);
                }
                catch (Exception ex)
                {
                    TempData["Message"] = "Wystąpił problem, spróbuj ponownie !";
                    return View();
                }
            }
            else
            {
                TempData["Message"] = "Nieprawidłowe dane !";
                return View();
            }
            
        }

        [HttpGet]
        public ActionResult ConfirmTrip(TripInformation tripParam)
        {
            return View(tripParam);
        }

        [HttpPost]
        public ActionResult ConfirmTripSave(TripInformation tripParam)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _managerDB.SaveTrip(new Concrete.Base.TripReservation()
                    {
                        CreateDate = DateTime.Now,
                        DateFromTo = String.Format("{0}-{1}", tripParam.DateFrom, tripParam.DateTo),
                        Description = tripParam.Description,
                        PeopleCount = Int32.Parse(tripParam.PeopleCount),
                        PersonReservation = tripParam.PersonReservation,
                        PlaceTrip = tripParam.PlaceTrip,
                        Price = Decimal.Parse(tripParam.TripCost),
                        TransportationType = GetTransportationType(tripParam.TransportationType).ToString(),
                        TripType = tripTypeC.ToString()
                    });
                    TempData["Message"] = "Dziękujemy za skorzystanie z naszej strony !";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["Message"] = "Wystąpił błąd zapisu do bazy!";
                    return View("ConfirmTrip", tripParam);
                }
            }
            else
            {
                TempData["Message"] = "Błędne dane!";
                return View("ConfirmTrip", tripParam);
            }

        }

        public PartialViewResult ShowTips( string transParam,string priceParam)
        {
            AdaptTips tips = new AdaptTips(tripTypeC, GetTransportationType(transParam));
            string result = tips.GetTips(decimal.Parse(priceParam));

            return PartialView((object)result);
        }

        /// <summary>
        /// Wybiera na podstawie wysłanych danych, sposób obliczania
        /// </summary>
        /// <param name="tansportParam"></param>
        /// <param name="kilometersParam"></param>
        /// <param name="personParam"></param>
        /// <returns></returns>
        private Calculation GetCalculationPattern(Transportation_type tansportParam, int kilometersParam,int personParam)
        {
            Calculation calculateMethod;

            switch (tansportParam)
            {
                case Transportation_type.BIKE:
                    calculateMethod = new BikeCalculation(kilometersParam,personParam);
                    break;
                case Transportation_type.BUS:
                    calculateMethod = new BusCalculation(kilometersParam, personParam);
                    break;
                case Transportation_type.CAR:
                    calculateMethod = new CarCalculation(kilometersParam, personParam);
                    break;
                case Transportation_type.PLANE:
                    calculateMethod = new PlaneCalculation(kilometersParam, personParam);
                    break;
                case Transportation_type.TRAIN:
                    calculateMethod = new TrainCalculation(kilometersParam, personParam);
                    break;
                default:
                    calculateMethod = new CarCalculation(kilometersParam, personParam);
                    break;
            }

            return calculateMethod;
        }

        /// <summary>
        /// Zwraca typ transportu w enum
        /// </summary>
        /// <param name="typeParam"></param>
        /// <returns></returns>
        private Transportation_type GetTransportationType(string typeParam)
        {
            switch (typeParam)
            {
                case "1":
                    return Transportation_type.CAR;
                case "2":
                    return Transportation_type.TRAIN;
                case "3":
                    return Transportation_type.PLANE;
                case "4":
                    return Transportation_type.BIKE;
                case "5":
                    return Transportation_type.BUS;
                default:
                    return Transportation_type.CAR;
            }
        }

        private Trip_type GetTripType(string typeParam)
        {
            if (typeParam == "1")
            {
                return Trip_type.COUNTRY;
            }
            else
            {
                return Trip_type.ABROAD;
            }
        }
    }
}