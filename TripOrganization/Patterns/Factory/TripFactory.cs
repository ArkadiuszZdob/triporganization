﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TripOrganization.Interfaces;
using TripOrganization.Concrete;
using TripOrganization.Models;

namespace TripOrganization.Patterns.Factory
{
    public class TripFactory
    {
        public TripFactory()
        { }

        public ITrip CreateTripManager(Trip_type tripParam)
        {
            if (tripParam == Trip_type.ABROAD)
            {
                return new AbroadTrip();
            }
            else if(tripParam == Trip_type.COUNTRY)
            {
                return new CountryTrip();
            }
            return null;
        }
    }
}