﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripOrganization.Patterns.Strategy
{
    public class BusCalculation : Calculation
    {
        private int _kilometers;
        private int _personCount;
        public BusCalculation(int kilometersParam, int personCountParam)
        {
            _kilometers = kilometersParam;
            _personCount = personCountParam;
        }

        public override decimal CalculateCost()
        {
            int busCount = _personCount / 15;
            if ((_personCount - (busCount * 15)) != 0)
            {
                ++busCount;
            }

            cost = (_kilometers * 5m) * busCount;

            return cost;
        }

        public override int CalculateTimeTrip()
        {
            timeInHour = _kilometers / 50;
            if ((_kilometers - (timeInHour * 50)) != 0)
                ++timeInHour;
            return timeInHour;
        }
    }
}