﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripOrganization.Patterns.Strategy
{
    public class BikeCalculation : Calculation
    {
        private int _kilometers;
        private int _personCount;
        public BikeCalculation(int kilometersParam, int personCountParam)
        {
            _kilometers = kilometersParam;
            _personCount = personCountParam;
        }

        public override decimal CalculateCost()
        {
            cost = _kilometers * 0.3m * _personCount;
            return cost;
        }

        public override int CalculateTimeTrip()
        {
            timeInHour = _kilometers / 15;
            if ((_kilometers - (timeInHour * 15)) != 0)
                ++timeInHour;
            return 0;
        }
    }
}