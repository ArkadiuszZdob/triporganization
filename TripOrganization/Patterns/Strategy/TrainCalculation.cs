﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripOrganization.Patterns.Strategy
{
    public class TrainCalculation : Calculation
    {
        private int _kilometers;
        private int _personCount;
        public TrainCalculation(int kilometersParam, int personCountParam)
        {
            _kilometers = kilometersParam;
            _personCount = personCountParam;
        }

        public override decimal CalculateCost()
        {
            int trainCabineCount = _personCount / 10;
            if ((_personCount - (trainCabineCount * 10)) != 0)
            {
                ++trainCabineCount;
            }

            cost = (_kilometers * 10m) * trainCabineCount;

            return cost;
        }

        public override int CalculateTimeTrip()
        {
            timeInHour = _kilometers / 100;
            if ((_kilometers - (timeInHour * 100)) != 0)
                ++timeInHour;
            return timeInHour;
        }
    }
}