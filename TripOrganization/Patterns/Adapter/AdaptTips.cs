﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TripOrganization.Interfaces;
using TripOrganization.Models;
using TripOrganization.Concrete;

namespace TripOrganization.Patterns.Adapter
{
    public class AdaptTips : TipsAnswer,IAdaptWrapper
    {
        public AdaptTips(Trip_type typeTripParam, Transportation_type type_transportParam) : base(typeTripParam,type_transportParam)
        {
        }

        string IAdaptWrapper.TakeTips(decimal priceParam)
        {
            return GetTips(priceParam);
        }
    }
}