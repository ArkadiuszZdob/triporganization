﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TripOrganization.Models
{
    public class TripInformation
    {
        [StringLength(10),Required(ErrorMessage ="Wymagane jest podanie ilości osób biorących udział"),RegularExpression(@".[0-9]*")]
        public string PeopleCount { get; set; }
        [Required(ErrorMessage ="Wymagane jest podanie środka transportu")]
        public string TransportationType { get; set; }
        [Required(ErrorMessage ="Wymagane jest podanie przewidywanego dnia przybicia"),
            RegularExpression(@"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$",ErrorMessage ="Błędny format daty!")]
        public string DateFrom { get; set; }
        [Required(ErrorMessage ="Wymagane jest podanie przewidywanej daty wyjazdu"),
            RegularExpression(@"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$", ErrorMessage = "Błędny format daty!")]
        public string DateTo { get; set; }
        [StringLength(50), Required(ErrorMessage ="Wymagane jest podanie miejsca wycieczki"),RegularExpression(@".[A-Za-z]*")]
        public string PlaceTrip { get; set; }
        [StringLength(400)]
        public string Description { get; set; }
        [StringLength(50), Required(ErrorMessage ="Wymagane jest nazwisko osoby rezerwującej"), RegularExpression(@".[A-Za-z]*")]
        public string PersonReservation { get; set; }
        [StringLength(10), Required(ErrorMessage ="Wymagane podanie jest szacowanej odległości"), RegularExpression(@".[0-9]*")]
        public string Kilometers { get; set; }
        [StringLength(12),Required(ErrorMessage ="Wymagane jest podanie numeru kontaktowego"), RegularExpression(@".[0-9]*")]
        public string Telephone { get; set; }
        public string TripCost { get; set; }
        public int TravelHour { get; set; }

    }
}