﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TripOrganization.Models
{
    public enum Trip_type
    {
        COUNTRY,
        ABROAD
    }

    public enum Transportation_type
    {
        CAR,
        PLANE,
        TRAIN,
        BIKE,
        BUS
    }
}