﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripOrganization.Patterns.Strategy;

namespace TripOrganization.Interfaces
{
    public interface ITrip
    {
        decimal CalculateTripPrice(Calculation calcParam,int personCount,int days);

        int CalculateTripTime(Calculation calcParam, int days);
    }
}
