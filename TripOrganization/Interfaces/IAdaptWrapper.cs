﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripOrganization.Models;

namespace TripOrganization.Interfaces
{
    public interface IAdaptWrapper
    {
        string TakeTips(decimal priceParam);
    }
}
